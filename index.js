//BUILT-IN ARRAY METHODS in JAVASCRIPT

// Mutator Methods - changes the array after they are created example add/remove data from array --------------------------
let fruits = ["Apple", "Orange", "Kiwi", "Dragonfruit"];

// push() -  add element in the end of aay and return array length

console.log("Current array:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits);

// Multiple push() into an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method:");
console.log(fruits);

// pop() removes the last element and returns the removed element.
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:");
console.log(fruits);


// unshift() - add one or more element in the beginning or start of Array
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:")
console.log(fruits);

//shift() - removes element in the beginning of array
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:");
console.log(fruits);

//splice() - simultaneously remove element from a specified index
fruits.splice(1, 2, "Lime", "Cherry"); //start insert in [1] then 2 means 2 elements babaguhin
console.log("Mutated array from splice method:");
console.log(fruits);

// sort() - rearrange array elements in alphanumeric order
fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);

// reverse() - reverse order of array elements
fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits);


// NON-MUTATOR Methods -  does not manipulate the original array contents----------------------------------------------

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

//indexOf() - works like search bar , first match in array: indexof(value) or indexof(value, startfromindex)
// Example 1
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);

//lastIndexOf() last matched element in an array
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method " + lastIndex); 

// lastIndexOf() start from specific index
let lastIndexStart = countries.lastIndexOf("PH", 6);
console.log("Result of lastIndexOf method " + lastIndexStart); 


// slice() - slice a portion from array and returns it as a new array
let slicedArrayA = countries.slice(2); //slice starting from index 2
console.log("Result from slice method 1:")
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4); //slice starting from index 2 until index 4
console.log("Result from slice method 2:")
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3); //if index is negative, slice 3 elements starting from the end
console.log("Result from slice method 3:")
console.log(slicedArrayC);

// toString() - returns array as string separated by comma
let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);

// concat() - combine to array and return combined result
let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result of concat method:");
console.log(tasks);

// combine multiple array using concat
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log("Result of multiple concat method:");
console.log(allTasks);

// combine elements to array using concat
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log("Result of element concat method:");
console.log(combinedTasks);

// join() - returns array as string separated by a specified separtor string
let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(users.join(' ! '));



//ITERATION METHODS -------------------------------------------------------------------
// Used to perform repetitive task to an array

// forEach() -  similar to for loop
allTasks.forEach( function(task){
	console.log(task);
});

	// using forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
});

console.log("Result of filtered tasks: ");
console.log(filteredTasks);

// map() - creates new array from calling a function for every array element
let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number){
	return number * number;
});

console.log("Original Array:");
console.log(numbers);
console.log("Mapped Array:")
console.log(numberMap);

// Difference of forEach() and map() = forEach does not require return statement;
let numberForEach = numbers.forEach(function(number){
	return number * number;
});

// every() - check if all elements meets the condition
// used for data validation for large volume data
let allValid = numbers.every(function(number){ //returns true or false
	return (number < 6);
});

console.log("Result of every method:");
console.log(allValid);

// some() - check if atleast 1 element meets the condition, returns true or false
let someValid = numbers.some(function(number){
	return (number < 6);
});

console.log("Result of some method:");
console.log(someValid);

// Use TRUTHY
if(someValid){
	console.log("Some numbers in the array are greater than 3");
};

// filter()
let filterValid = numbers.filter(function(number){
	return (number < 4);
});

console.log("Result from filter method:");
console.log(filterValid);

// No element found - if the filter condition has no met elements, it will return blank array
let nothingFound = numbers.filter(function(number){
	return (number = 0);
});

console.log("Result from filter method:");
console.log(nothingFound);

// filtering using forEach()
let filteredNumbers = [];
numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
});

console.log("Result from filter method using forEach:");
console.log(filteredNumbers);

// includes() - checks if the argument exists inside the array elements, returns true or false
let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];
let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

// reduce() - 
let iteration = 0;
let reduceArray = numbers.reduce(function(x, y){//x is accumulator, y is value
	console.warn("Current Iteration: "+ ++iteration);
	console.log("accumulator: "+ x);
	console.log("currentValue: "+ y);

	return x + y;
});

console.log("Result of reduce method: " + reduceArray);